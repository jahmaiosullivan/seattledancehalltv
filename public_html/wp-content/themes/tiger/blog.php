<?php
/*
Template Name: Blog
*/
?>
<?php get_header(); ?>

<div id="container">
  <div id="content">
    <?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
    <div id="nav-above" class="navigation">
      <p class="nav-previous">
        <?php next_posts_link(__( '<span class="meta-nav">&laquo;</span> older articles', 'tiger' )) ?>
      </p>
      <p class="nav-next">
        <?php previous_posts_link(__( 'newer articles <span class="meta-nav">&raquo;</span>', 'tiger' )) ?>
      </p>
    </div>
    <?php } ?>
    <?php
$temp = $wp_query;
$wp_query= null;
$wp_query = new WP_Query();
$tiger_number_posts = get_option('tiger_number_posts');
$wp_query->query('posts_per_page='.$tiger_number_posts.'&paged='.$paged);
while ($wp_query->have_posts()) : $wp_query->the_post();
?>
    <div id="post-<?php the_ID(); ?>" class='<?php echo (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>'>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
      <?php tiger_truncate_title(20); ?>
      </a></h2>
    <?php $tiger_thumbs_blog = get_option('tiger_thumbs_blog');
if ($tiger_thumbs_blog == "true"): ?>
    <?php if ( has_post_thumbnail() ): $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_post_thumbnail( 'blog-thumb' ); ?></a>
    <?php else: ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><img src="<?php echo get_template_directory_uri(); ?>/images/awaiting-image.jpg" alt="Awaiting image" class="blog-thumb" /></a>
    <?php endif; ?>
    <?php endif; ?>
    <div class="entry-meta"> <span class="meta-prep meta-prep-author">
      <?php _e('By ', 'tiger'); ?>
      </span> <span class="author vcard"><a class="url fn n" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" title="<?php printf( __( 'View all articles by %s', 'tiger' ), $authordata->display_name ); ?>">
      <?php the_author(); ?>
      </a></span> <span class="meta-sep"> | </span> <span class="meta-prep meta-prep-entry-date">
      <?php _e('Published ', 'tiger'); ?>
      </span> <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>">
      <?php the_time( get_option( 'date_format' ) ); ?>
      </abbr></span>
      <?php edit_post_link( __( 'Edit', 'tiger' ), "<span class=\"meta-sep\"> | </span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ) ?>
    </div>
    <div class="entry-content">
      <?php $tiger_thumbs_blog = get_option('tiger_thumbs_blog');
if ($tiger_thumbs_blog == "true"): ?>
      <?php tiger_truncate_post(320); ?>
      <?php else: ?>
      <?php tiger_truncate_post(650); ?>
      <?php endif; ?>
    </div>
  </div>
    <?php comments_template(); ?>
    <?php endwhile; ?>
    <?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
    <div id="nav-below" class="navigation">
      <?php if(function_exists('wp_pagenavi')) { ?>
      <?php wp_pagenavi(); ?>
      <?php } else { ?>
      <div class="nav-previous">
        <?php next_posts_link(__( '<span class="meta-nav">&laquo;</span> older articles', 'tiger' )) ?>
      </div>
      <div class="nav-next">
        <?php previous_posts_link(__( 'newer articles <span class="meta-nav">&raquo;</span>', 'tiger' )) ?>
      </div>
      <?php }  ?>
    </div>
    <div class="clear-floats"></div>
    <?php } ?>
  </div>
</div>
<div id="sidebar">
  <?php dynamic_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
