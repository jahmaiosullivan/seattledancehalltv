jQuery(document).ready(function($){
	$("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);  
});
jQuery(document).ready(function($) {

	$('#to-top').click(function() {
		
		$('html, body').animate({ scrollTop : 0 }, 'slow');
		return false;
		
	});

});
var inputObject = {
	widthHoverPlus : 65,
	goLarge : function(obj) {	
		if (!obj.hasClass('wide')) {
			obj.animate({ 'width' : (obj.width() + parseInt(this.widthHoverPlus, 10)) + 'px', }, 'fast');
		}
	},
	goSmall : function(obj) {
		if (obj.val() === '') {
			obj.animate({ 'width' : (obj.width() - parseInt(this.widthHoverPlus, 10)) + 'px' }, 'fast');	
			if (obj.hasClass('wide')) {
				obj.removeClass('wide');
			}
		} else {
			if (!obj.hasClass('wide')) {
				obj.addClass('wide');
			}
		}
	}
}
jQuery(document).ready(function($) {
	
	$('#s').focus(function() {
		inputObject.goLarge($(this));
	});
	
	$('#s').blur(function() {
		inputObject.goSmall($(this));
	});
	
});