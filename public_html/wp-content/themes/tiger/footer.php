</div>
</div>
<div id="footerwrapper">
  <div class="center">
    <div id="footerleft">&copy; <?php echo date("Y") ?>
      <?php bloginfo( 'name' ) ?>. All Rights Reserved.<br />
      Powered by <a href="http://wordpress.org" title="WordPress" target="_blank">WordPress</a>. Tiger theme by <a href="http://www.jocoxdesign.co.uk" title="Jo Cox Design" target="_blank">Jo Cox Design</a></div>
    <div id="footerright"><a href="#" id="to-top">Return to top &uarr;</a></div>
  </div>
</div>
<?php wp_footer(); ?>
</body></html>