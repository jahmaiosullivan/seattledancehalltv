=== THEME NAME ===

Tiger 1.7 WordPress Theme

=== DESCRIPTION ===

A clean and simple orange/dark grey theme with optional site logo and favicon upload, breadcrumbs, latest posts slider, social networking icon and Google Analytics intergration via the Theme Options. Also includes separate blog and full-width page templates, support for the Wp-PageNavi plugin, threaded comments, post thumbnails, drop down navigation and integrated search.

=== LICENSE ===

GNU General Public License | http://www.gnu.org/licenses/gpl-2.0.html

=== SUPPORT & DOCUMENTATION ===

This theme is fully documented and supported at:

http://www.jocoxdesign.co.uk/wordpress-themes/tiger