<?php
if ( post_password_required() ) { ?>

<p class="nocomments">
  <?php _e('This post is password protected. Enter the password to view comments.', 'tiger'); ?>
</p>
<?php
	return; }

if ( have_comments() ) : ?>
<h3 id="comments">
  <?php comments_number('No Responses', 'One Response', '% Responses');?>
  <?php printf('to "%s"', the_title('', '', false)); ?></h3>
<div class="navigation">
  <div class="alignleft">
    <?php previous_comments_link() ?>
  </div>
  <div class="alignright">
    <?php next_comments_link() ?>
  </div>
</div>
<ol class="commentlist">
  <?php
     wp_list_comments(array('type' => 'comment'));
      ?>
</ol>
<?php if ( ! empty($comments_by_type['pings']) ): ?>
<h3>Trackbacks</h3>
<ol class="commentlist">
  <?php
     wp_list_comments(array('type' => 'pings'));
      ?>
</ol>
<?php endif; ?>
<div class="navigation">
  <div class="alignleft">
    <?php previous_comments_link() ?>
  </div>
  <div class="alignright">
    <?php next_comments_link() ?>
  </div>
</div>
<?php
endif;

comment_form(array(

  'comment_field' => '<p><textarea name="comment" id="comment" cols="58" rows="10" tabindex="4" aria-required="true"></textarea></p>',
  'label_submit' => 'Submit Comment',
  'comment_notes_after' => ''
  ));

echo '<div class="comments_rss">';
post_comments_feed_link('Comments RSS Feed');
echo '</div>';

?>
