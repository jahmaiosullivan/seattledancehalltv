<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title>
<?php wp_title(' | ', true, 'right'); ?>
<?php bloginfo('name'); ?>
</title>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="topwrapper">
  <div class="center">
    <div id="header">
      <div id="headleft">
        <div id="blog-title">
          <?php $tiger_sitelogo = get_option('tiger_sitelogo');	if ($tiger_sitelogo): ?>
          <a href="<?php echo home_url() ?>/" title="<?php bloginfo( 'name' ) ?>" rel="home"><img src="<?php echo $tiger_sitelogo; ?>" /></a>
          <?php else: ?>
          <a href="<?php echo home_url() ?>/" title="<?php bloginfo( 'name' ) ?>" rel="home">
          <?php bloginfo( 'name' ) ?>
          </a>
          <?php endif; ?>
        </div>
        <?php $tiger_strapline = get_option('tiger_strapline');
		if ($tiger_strapline == "true"): ?>
        <div id="blog-description">
          <?php bloginfo( 'description' ) ?>
        </div>
        <?php endif; ?>
      </div>
      <div id="headright">
        <div id="searchbox">
          <?php $tiger_twitter = get_option('tiger_twitter'); if ($tiger_twitter) { ?>
          <?php echo "<a href=\"http://$tiger_twitter\" target=\"_blank\"><img src=\"". get_template_directory_uri() . "/images/twitter_32.png\" alt=\"Twitter\"/></a>"; ?>
          <?php } ?>
          <?php $tiger_facebook = get_option('tiger_facebook'); if ($tiger_facebook) { ?>
          <?php echo "<a href=\"http://$tiger_facebook\" target=\"_blank\"><img src=\"". get_template_directory_uri() . "/images/facebook_32.png\" alt=\"Facebook\"/></a>"; ?>
          <?php } ?>
          <?php $tiger_linkedin = get_option('tiger_linkedin'); if ($tiger_linkedin) { ?>
          <?php echo "<a href=\"http://$tiger_linkedin\" target=\"_blank\"><img src=\"". get_template_directory_uri() . "/images/linkedin_32.png\" alt=\"Linkedin\"/></a>"; ?>
          <?php } ?>
          <?php $tiger_youtube = get_option('tiger_youtube'); if ($tiger_youtube) { ?>
          <?php echo "<a href=\"http://$tiger_youtube\" target=\"_blank\"><img src=\"". get_template_directory_uri() . "/images/youtube_32.png\" alt=\"Youtube\"/></a>"; ?>
          <?php } ?>
          <?php $tiger_rss = get_option('tiger_rss'); if ($tiger_rss) { ?>
          <?php echo "<a href=\"http://$tiger_rss\" target=\"_blank\"><img src=\"". get_template_directory_uri() . "/images/rss_32.png\" alt=\"RSS\"/></a>"; ?>
          <?php } ?>
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="navwrapper">
  <div class="center">
    <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_class' => 'menu', 'menu_class' => '' ) ); ?>
  </div>
</div>
<div id="midwrapper">
<div class="center">
<?php if (is_front_page()): ?>
<?php $tiger_slider = get_option('tiger_slider');
if ($tiger_slider == "true"): ?>
<div id="featured" >
  <ul class="ui-tabs-nav">
    <?php
global $post;
$args = array( 'numberposts' => 1 );
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>
    <li class="ui-tabs-nav-item ui-tabs-selected" id="nav-fragment-<?php echo $post->ID; ?>"><a href="#fragment-<?php echo $post->ID; ?>">
      <?php if ( has_post_thumbnail() ): $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
      <?php the_post_thumbnail(array( 120,60,true )); ?>
      <?php else: ?>
      <img src="<?php echo get_template_directory_uri(); ?>/images/awaiting-slider-thb.jpg" alt="Awaiting image" />
      <?php endif; ?>
      <?php tiger_truncate_title(20); ?>
      </a></li>
    <?php endforeach; ?>
    <?php
global $post;
$args = array( 'numberposts' => 4, 'offset'=> 1 );
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>
    <li class="ui-tabs-nav-item" id="nav-fragment-<?php echo $post->ID; ?>"><a href="#fragment-<?php echo $post->ID; ?>">
      <?php if ( has_post_thumbnail() ): $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
      <?php the_post_thumbnail(array( 120,60,true )); ?>
      <?php else: ?>
      <img src="<?php echo get_template_directory_uri(); ?>/images/awaiting-slider-thb.jpg" alt="Awaiting image" />
      <?php endif; ?>
      <span>
      <?php tiger_truncate_title(20); ?>
      </span></a></li>
    <?php endforeach; ?>
  </ul>
  <?php
global $post;
$args = array( 'numberposts' => 1 );
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>
  <div id="fragment-<?php echo $post->ID; ?>" class="ui-tabs-panel" style="">
    <?php if ( has_post_thumbnail() ): $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"> <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
    <?php the_post_thumbnail(array( 640,350,true )); ?>
    </a>
    <?php else: ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><img src="<?php echo get_template_directory_uri(); ?>/images/awaiting-slider.jpg" alt="Awaiting image" class="blog-thumb" /></a>
    <?php endif; ?>
    <div class="info" >
      <h2><a href="#" >
        <?php tiger_truncate_title(50); ?>
        </a></h2>
      <p>
        <?php tiger_truncate_post(320); ?>
      </p>
    </div>
  </div>
  <?php endforeach; ?>
  <?php
global $post;
$args = array( 'numberposts' => 4, 'offset'=> 1 );
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>
  <div id="fragment-<?php echo $post->ID; ?>" class="ui-tabs-panel ui-tabs-hide" style="">
    <?php if ( has_post_thumbnail() ): $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"> <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
    <?php the_post_thumbnail(array( 640,350,true )); ?>
    </a>
    <?php else: ?>
    <a href="<?php the_permalink(); ?>" title="<?php printf( __('Read', 'tiger'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><img src="<?php echo get_template_directory_uri(); ?>/images/awaiting-slider.jpg" alt="Awaiting image" class="blog-thumb" /></a>
    <?php endif; ?>
    <div class="info" >
      <h2><a href="#" >
        <?php tiger_truncate_title(50); ?>
        </a></h2>
      <p>
        <?php tiger_truncate_post(320); ?>
      </p>
    </div>
  </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>
<?php endif; ?>
<?php $tiger_breadcrumbs = get_option('tiger_breadcrumbs'); if ($tiger_breadcrumbs == "true"): ?>
<?php tiger_breadcrumbs(); ?>
<?php endif; ?>
