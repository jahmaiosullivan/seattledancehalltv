<?php get_header(); ?>
<div id="content">
  <?php the_post(); ?>
  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>
    <div class="entry-meta"> <span class="meta-prep meta-prep-author">
      <?php _e('By ', 'tiger'); ?>
      </span> <span class="author vcard"><a class="url fn n" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" title="<?php printf( __( 'View all articles by %s', 'tiger' ), $authordata->display_name ); ?>">
      <?php the_author(); ?>
      </a></span> <span class="meta-sep"> | </span> <span class="meta-prep meta-prep-entry-date">
      <?php _e('Published ', 'tiger'); ?>
      </span> <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>">
      <?php the_time( get_option( 'date_format' ) ); ?>
      </abbr></span>
      <?php edit_post_link( __( 'Edit', 'tiger' ), "<span class=\"meta-sep\"> | </span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ) ?>
    </div>
    <div class="entry-content">
      <?php the_content(); ?>
      <div class="clear-floats"></div>
      <?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'tiger' ) . '&after=</div>') ?>
    </div>
    <div class="entry-utility"> <?php printf( __( 'This article was posted in %1$s%2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. Follow comments with the <a href="%5$s" title="Comments RSS to %4$s" rel="alternate" type="application/rss+xml">RSS feed for this post</a>. ', 'tiger' ),
get_the_category_list(', '),
get_the_tag_list( __( ' and tagged ', 'tiger' ), ', ', '' ),
get_permalink(),
the_title_attribute('echo=0'),
get_post_comments_feed_link() ) ?>
      <?php if ( ('open' == $post->comment_status) && ('open' == $post->ping_status) ) : // ?>
      <?php printf( __( '<a class="comment-link" href="#respond" title="Post a Comment">Post a Comment</a> or leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for your post" rel="trackback">Trackback URL</a>.', 'tiger' ), get_trackback_url() ) ?>
      <?php elseif ( !('open' == $post->comment_status) && ('open' == $post->ping_status) ) : // ?>
      <?php printf( __( 'Comments are closed, but you can leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for post" rel="trackback">Trackback URL</a>.', 'tiger' ), get_trackback_url() ) ?>
      <?php elseif ( ('open' == $post->comment_status) && !('open' == $post->ping_status) ) : // ?>
      <?php _e( 'Trackbacks are closed, but you can <a class="comment-link" href="#respond" title="Post a Comment">Post a Comment</a>.', 'tiger' ) ?>
      <?php elseif ( !('open' == $post->comment_status) && !('open' == $post->ping_status) ) : // ?>
      <?php _e( 'Both comments and trackbacks are closed.', 'tiger' ) ?>
      <?php endif; ?>
      <?php edit_post_link( __( 'Edit', 'tiger' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ) ?>
    </div>
  </div>
  <div id="nav-below" class="navigation">
    <div class="nav-previous">
      <?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' ) ?>
    </div>
    <div class="nav-next">
      <?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ) ?>
    </div>
  </div>
  <div class="clear-floats"></div>
  <?php comments_template('', true); ?>
</div>
<div id="sidebar">
  <?php dynamic_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
