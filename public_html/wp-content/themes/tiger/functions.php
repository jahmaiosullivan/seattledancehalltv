<?php

require_once(get_template_directory() . '/admin/admin-functions.php');
require_once(get_template_directory() . '/admin/admin-interface.php');
require_once(get_template_directory() . '/admin/theme-settings.php');

/* ENQUE SCRIPTS */
function tiger_scripts() {
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'jquery-ui-core' );
  wp_enqueue_script( 'jquery-ui-tabs' );
  wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/tiger.js', 'jquery' );
}
add_action('init', 'tiger_scripts');

/* SET CONTENT WIDTH */
if ( ! isset( $content_width ) ) $content_width = 640;

/* ADD EDITOR STYLE SUPPORT */
add_theme_support('editor_style');
add_editor_style('editor-style.css');

/* ENABLE AUTOMATIC FEED LINKS */
add_theme_support( 'automatic-feed-links' );

/* ENABLE THUMBNAILS */
add_theme_support( 'post-thumbnails' );
add_image_size( 'blog-thumb', 300, 164, true );  

/* REGISTER PRIMARY MENU */
function tiger_register_menus() {

	register_nav_menus( array(
		'main-menu' => __( 'Primary Navigation', 'tiger' ),
	) );
 }	
	
add_action ('init', 'tiger_register_menus' );

/* ADD ANALYTICS CODE TO WP_HEAD */
add_action('wp_head', 'tiger_googleanalytics');
function tiger_googleanalytics() { 
$tiger_google_analytics = get_option('tiger_google_analytics');
echo $tiger_google_analytics;
} 

/* ADD FAVICON CODE TO WP_HEAD */
add_action('wp_head', 'tiger_favicon');
function tiger_favicon() { 
$tiger_favicon = get_option('tiger_favicon');
if ($tiger_favicon):
echo '<link rel="shortcut icon" type="image/x-icon" href="'.$tiger_favicon.'" />';
endif;
} 

/* REGISTER SIDEBAR */
add_action( 'widgets_init', 'tiger_sidebars' );
function tiger_sidebars() {
register_sidebar(
		array(
			'id' => 'right',
			'name' => __( 'Right Sidebar', 'tiger'  ),
			'description' => __( 'The sidebar which appears in the right hand column', 'tiger'  ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</li>",
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		)
	);
}

/* TRUNCATE TITLES */
function tiger_truncate_title($amount,$echo=true) {
	$truncate = get_the_title(); 
	if ( strlen($truncate) <= $amount ) $echo_out = ''; else $echo_out = '...';
	$truncate = substr( $truncate, 0, $amount );
	if ($echo) {
		echo $truncate;
		echo $echo_out;
	}
	else { return ($truncate . $echo_out); }
}

/* TRUNCATE POST EXCERPTS */
function tiger_truncate_post($amount,$quote_after=false) {
	$truncate = get_the_content(); 
	$truncate = apply_filters('the_content', $truncate);
	$truncate = preg_replace('@<script[^>]*?>.*?</script>@si', '', $truncate);
	$truncate = preg_replace('@<style[^>]*?>.*?</style>@si', '', $truncate);
	$truncate = strip_tags($truncate);
	$truncate = substr($truncate, 0, strrpos(substr($truncate, 0, $amount), ' ')); 
	echo $truncate;
	echo "...";
	if ($quote_after) echo('"');
}

/* BREADCRUMBS */
function tiger_breadcrumbs() {
 
 $showOnHome = 0; 
 $delimiter = '&gt;'; 
 $home = 'Home'; 
 $showCurrent = 1; 
 $before = '<span class="current">'; 
 $after = '</span>'; 
 
  global $post;
  $homeLink = home_url();
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '
<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>

';
 
  } else {
 
    echo '
<div id="crumbs">
<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
 if ( is_category() ) {
 global $wp_query;
 $cat_obj = $wp_query->get_queried_object();
 $thisCat = $cat_obj->term_id;
 $thisCat = get_category($thisCat);
 $parentCat = get_category($thisCat->parent);
 if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
 echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
 
 } elseif ( is_search() ) {
 echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
 } elseif ( is_day() ) {
 echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
 echo $before . get_the_time('d') . $after;
 
 } elseif ( is_month() ) {
 echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 echo $before . get_the_time('F') . $after;
 
 } elseif ( is_year() ) {
 echo $before . get_the_time('Y') . $after;
 
 } elseif ( is_single() && !is_attachment() ) {
 if ( get_post_type() != 'post' ) {
 $post_type = get_post_type_object(get_post_type());
 $slug = $post_type->rewrite;
 echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
 if ($showCurrent == 1) echo $before . get_the_title() . $after;
 } else {
 $cat = get_the_category(); $cat = $cat[0];
 echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 if ($showCurrent == 1) echo $before . get_the_title() . $after;
 }
 
 } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
 $post_type = get_post_type_object(get_post_type());
 echo $before . $post_type->labels->singular_name . $after;
 
 } elseif ( is_attachment() ) {
 $parent = get_post($post->post_parent);
 $cat = get_the_category($parent->ID); $cat = $cat[0];
 echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
 if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
 } elseif ( is_page() && !$post->post_parent ) {
 if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
 } elseif ( is_page() && $post->post_parent ) {
 $parent_id = $post->post_parent;
 $breadcrumbs = array();
 while ($parent_id) {
 $page = get_page($parent_id);
 $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
 $parent_id = $page->post_parent;
 }
 $breadcrumbs = array_reverse($breadcrumbs);
 foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
 if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
 } elseif ( is_tag() ) {
 echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
 } elseif ( is_author() ) {
 global $author;
 $userdata = get_userdata($author);
 echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
 } elseif ( is_404() ) {
 echo $before . 'Error 404' . $after;
 }
 
 if ( get_query_var('paged') ) {
 if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
 echo __('Page', 'tiger') . ' ' . get_query_var('paged');
 if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
 }
 
 echo '</div>
';
 
  }
}

?>