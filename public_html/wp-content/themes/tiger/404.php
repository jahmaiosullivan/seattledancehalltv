<?php get_header(); ?>
<div id="container">
<div id="content">
<div id="post-0" class="post error404 not-found">
<h1 class="entry-title">Not Found</h1>
<div class="entry-content">
<p>Nothing here, sorry!</p>
</div>
</div>
</div>
</div>
<div id="sidebar"><?php dynamic_sidebar('right'); ?></div>
<?php get_footer(); ?>