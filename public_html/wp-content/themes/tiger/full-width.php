<?php
/*
Template Name: Full Width
*/
?>
<?php get_header(); ?>
<div id="content-full-width">
<?php the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h1 class="entry-title"><?php the_title(); ?></h1>
<div class="entry-content">
<?php the_content(); ?>
<?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'tiger' ) . '&after=</div>') ?>
<?php edit_post_link( __( 'Edit', 'tiger' ), '<span class="edit-link">', '</span>' ) ?>
</div>
</div>
<?php comments_template( '', true ); ?>
</div>


<?php get_footer(); ?>