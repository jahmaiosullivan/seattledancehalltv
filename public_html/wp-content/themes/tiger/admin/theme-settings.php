<?php

add_action('init','of_options');

if (!function_exists('of_options')) {
function of_options(){

//Theme Name
$themename ="Tiger";


//Theme Shortname
$shortname = "tiger";


//Populate the options array
global $tt_options;
$tt_options = get_option('of_options');


// Options: number of posts to show on blog page
$blog_number_posts = array("2","4","6","8","10");


/*-----------------------------------------------------------------------------------*/
/* Create The Custom Site Options Panel
/*-----------------------------------------------------------------------------------*/
$options = array();


/* Option Page 1 - General Settings */	
$options[] = array( "name" => __('General Settings','framework_localize'),
			"type" => "heading");
			
			
$options[] = array( "name" => __('Website Logo','framework_localize'),
			"desc" => __('Upload a custom logo for your Website.','framework_localize'),
			"id" => $shortname."_sitelogo",
			"std" => "",
			"type" => "upload");
			
			
$options[] = array( "name" => __('Strapline','framework_localize'),
			"desc" => __('Use blog description as strapline?','framework_localize'),
			"id" => $shortname."_strapline",
			"std" => "true",
			"type" => "checkbox");


$options[] = array( "name" => __('Breadcrumbs','framework_localize'),
			"desc" => __('Show breadcrumbs?','framework_localize'),
			"id" => $shortname."_breadcrumbs",
			"std" => "true",
			"type" => "checkbox");
			
			
$options[] = array( "name" => __('Post Thumbnails','framework_localize'),
			"desc" => __('Enable post thumbnails on blog and home pages?','framework_localize'),
			"id" => $shortname."_thumbs_blog",
			"std" => "true",
			"type" => "checkbox");


$options[] = array( "name" => __('Latest Posts Slider','framework_localize'),
			"desc" => __('Enable latest posts slider on the homepage?','framework_localize'),
			"id" => $shortname."_slider",
			"std" => "true",
			"type" => "checkbox");
			
			
$options[] = array( "name" => __('Number of posts displayed on Blog page','framework_localize'),
			"desc" => __('','framework_localize'),
			"id" => $shortname."_number_posts",
			"std" => "1",
			"type" => "select",
			"options" => $blog_number_posts);
			
			
$options[] = array( "name" => __('Favicon','framework_localize'),
			"desc" => __('Upload a 16px x 16px image that will represent your website\'s favicon.<br /><br /><em>To ensure cross-browser compatibility, we recommend converting the favicon into .ico format before uploading. (<a href="http://www.favicon.cc/">www.favicon.cc</a>)</em>','framework_localize'),
			"id" => $shortname."_favicon",
			"std" => "",
			"type" => "upload");
			
									   
$options[] = array( "name" => __('Tracking Code','framework_localize'),
			"desc" => __('Paste Google Analytics (or other) tracking code here.','framework_localize'),
			"id" => $shortname."_google_analytics",
			"std" => "",
			"type" => "textarea");


/* Option Page 2 - Social */
$options[] = array( "name" => __('Social Networks','framework_localize'),
			"type" => "heading");
			

$options[] = array( "name" => __('Twitter','framework_localize'),
			"desc" => "Your Twitter profile URL",
			"id" => $shortname."_twitter",
			"std" => "",
			"type" => "text");


$options[] = array( "name" => __('Facebook','framework_localize'),
			"desc" => "Your Facebook profile/page URL",
			"id" => $shortname."_facebook",
			"std" => "",
			"type" => "text");
			

$options[] = array( "name" => __('Linkedin','framework_localize'),
			"desc" => "Your Linkedin profile URL",
			"id" => $shortname."_linkedin",
			"std" => "",
			"type" => "text");
				

$options[] = array( "name" => __('Youtube','framework_localize'),
			"desc" => "Your Youtube channel/profile URL",
			"id" => $shortname."_youtube",
			"std" => "",
			"type" => "text");		
			
			
$options[] = array( "name" => __('RSS','framework_localize'),
			"desc" => "Your RSS feed URL",
			"id" => $shortname."_rss",
			"std" => "",
			"type" => "text");							
					


update_option('of_template',$options); 					  
update_option('of_themename',$themename);   
update_option('of_shortname',$shortname);

}
}
?>